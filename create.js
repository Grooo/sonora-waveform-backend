const fs = require('fs')
// -
const ffmpegPath = require('@ffmpeg-installer/ffmpeg')
const ffmpeg = require('fluent-ffmpeg')
ffmpeg.setFfmpegPath(ffmpegPath.path);
// -
const { createCanvas } = require('canvas')
// -
const AudioContext = require('web-audio-api').AudioContext;
const audioCtx = new AudioContext();
// - 
const args = process?.argv?.slice(2);


// - Presets
const preset = args[9] || '3';
// - Multiplier x 8.0, points * 0.5
const multiplierProp  = 4.0;
// - 
const presets = {
	waves3: {
		layers: 			Number(args[6] || 7),
		points: 			18,
		sinusoidal: 		0.15913,
		sinusoidalPercent: 	0.9,
		tension: 			0.5,
		incr: 				0.010,
		multiplier: 		0.68 * multiplierProp,
		colorRGBA: 			"rgba(255,0,0, 1.0)",
		gradient: 			0.5,
		gradient2: 			1,
		colorRGBA2: 		"rgba(255,0,107, 0.04)",
		lineColor: 			"#fc0000",
		lineAlpha: 			0,
		lineColorRGBA:	 	"rgba(252,0,0, 0)",
		lineWidth: 			0.5,
		blendMode: 			"source-over"
	},
	shadow3: {
		blur: 				30,
		distance: 			3,
		colorRGBA: 			"rgba(247,1,89, 0.15)"
	},

	waves4: {
		layers: 			Number(args[6] || 7),
		points: 			35,
		sinusoidal: 		0.15757000000000002,
		sinusoidalPercent: 	0.7609,
		tension: 			0.5,
		incr: 				0.0071400000000000005,
		multiplier: 		0.46683 * multiplierProp,
		color: 				"#ff0000",
		alpha: 				1,
		colorRGBA: 			"rgba(255,0,0, 1)",
		gradient: 			0,
		gradient2: 			1,
		color2: 			"#ff0f00",
		alpha2: 			0.095,
		colorRGBA2: 		"rgba(255,15,0, 0.095)",
		lineColor: 			"#ff0000",
		lineAlpha: 			0,
		lineColorRGBA: 		"rgba(255,0,0, 0)",
		lineWidth: 			0,
		blendMode: 			"lighter"
	},
	shadow4: {
		blur: 				34,
		distance: 			1.8,
		colorRGBA: 			"rgba(242,6,70, .1)"
	}
}

// -
let opts = {
	fps: 					Number(args[3] || 25),
	delay: 					1,
	bitrate: 				'2048k',
	removeTmpFolder: 		false,
	iniTime: 				Number(args[1] || 0),
	endTime: 				Number(args[2] || 10),
	oWidth: 				Number(args[4] || 256),
	oHeight: 				Number(args[5] || 128),
	scale: 					Number(args[7] || 1),
	velUp: 					0.9,
	velDown: 				0.1,

	waves: 					presets[`waves${preset}`],

	shadow: 				presets[`shadow${preset}`],
	blur: 					Number(args[8] || 0.0),
	volume: 				1
}

// - 
opts.width = Math.floor(opts.oWidth * opts.scale);
opts.height = Math.floor(opts.oHeight * opts.scale);

// -
let points = [],
	l = 0,
	percentFrequency = 0;
// -
let shapeIncr 	= 0,
	posX 		= 0,
	extraY  	= 0;


let frame = 0,
	dataIni = null,
	dataIniConvertVideo = null,
	dataIniDecode = null,
	dataEnd = null,
	outputFile = '',
	samplerate = 0;

// - Canvas
const canvas = createCanvas( opts.width, opts.height);
const ctx = canvas.getContext('2d');
ctx.quality = 'good';
ctx.antialias = 'subpixel';

// - 
let incrPoint = opts.width / (opts.waves.points-1);

// - Waves
let waves = [];
// -
for(let i=0; i<opts.waves.layers; i++){
	const totalPoints = (opts.waves.points+2)*2;
	points = new Array( totalPoints );
	points.fill(opts.height);
	// -
	points[ totalPoints-4] = opts.width;
	points[ totalPoints-3] = opts.height;
	points[ totalPoints-2] = 0;
	points[ totalPoints-1] = opts.height;
	// -
	waves.push({
		points: points,
		proxyPoints: [...points]
	});
}


// - Create folder
const tmpFolder = `${__dirname}/tmp`;
if ( !fs.existsSync(tmpFolder)) fs.mkdirSync(tmpFolder);

const videosFolder = `${__dirname}/videos`;
if ( !fs.existsSync(videosFolder)) fs.mkdirSync(videosFolder);

const imgFolder = `${__dirname}/img`;
if ( !fs.existsSync(imgFolder)) fs.mkdirSync(imgFolder);

// Background image
const bgImg = args[10] ? args[10] : null;


// - DRAW -------------------------------------------
function drawCurvePath(ctx, pts, _tension, _isClosed, _numOfSegments, _showPoints) {
    // -
    let tension         = _tension          || 0.5;
    let isClosed        = _isClosed         || false;
    let numOfSegments   = _numOfSegments    || 16;
    let showPoints      = _showPoints       || false;
    // -
    ctx.beginPath();
    // -
    drawLines(ctx, getCurvePoints(pts, tension, isClosed, numOfSegments));
    // -
    if (showPoints) {
        ctx.stroke();
        ctx.beginPath();
        // -
        for( let i=0; i<pts.length-1; i+=2 ){
            ctx.rect(pts[i]-2, pts[i+1]-2, 4, 4);
        }
    }
    // -
    ctx.fill();
}

function getCurvePoints(pts, _tension, _isClosed, _numOfSegments) {
    // - Use input value if provided, or use a default value   
    let tension         = _tension          || 0.5;
    let isClosed        = _isClosed         || false;
    let numOfSegments   = _numOfSegments    || 16;

    let _pts = [], res = [],// clone array
        x, y,               // our x,y coords
        t1x, t2x, t1y, t2y, // tension vectors
        c1, c2, c3, c4,     // cardinal points
        st, t, i;           // steps based on num. of segments

    // clone array so we don't change the original
    _pts = pts.slice(0);

    // The algorithm require a previous and next point to the actual point array.
    // Check if we will draw closed or open curve.
    // If closed, copy end points to beginning and first points to end
    // If open, duplicate first points to befinning, end points to end
    if ( isClosed ) {
        _pts.unshift(pts[pts.length - 1]);
        _pts.unshift(pts[pts.length - 2]);
        _pts.unshift(pts[pts.length - 1]);
        _pts.unshift(pts[pts.length - 2]);
        _pts.push(pts[0]);
        _pts.push(pts[1]);
    
    }else{
        _pts.unshift(pts[1]);           //copy 1. point and insert at beginning
        _pts.unshift(pts[0]);
        _pts.push(pts[pts.length - 2]); //copy last point and append
        _pts.push(pts[pts.length - 1]);
    }

    // ok, lets start..

    // 1. loop goes through point array
    // 2. loop goes through each segment between the 2 pts + 1e point before and after
    for (i=2; i < (_pts.length - 4); i+=2) {
        for (t=0; t <= numOfSegments; t++) {

            // calc tension vectors
            t1x = (_pts[i+2] - _pts[i-2]) * tension;
            t2x = (_pts[i+4] - _pts[i]) * tension;

            t1y = (_pts[i+3] - _pts[i-1]) * tension;
            t2y = (_pts[i+5] - _pts[i+1]) * tension;

            // calc step
            st = t / numOfSegments;

            // calc cardinals
            c1 =   2 * Math.pow(st, 3)  - 3 * Math.pow(st, 2) + 1; 
            c2 = -(2 * Math.pow(st, 3)) + 3 * Math.pow(st, 2); 
            c3 =       Math.pow(st, 3)  - 2 * Math.pow(st, 2) + st; 
            c4 =       Math.pow(st, 3)  -     Math.pow(st, 2);

            // calc x and y cords with common control vectors
            x = c1 * _pts[i]    + c2 * _pts[i+2] + c3 * t1x + c4 * t2x;
            y = c1 * _pts[i+1]  + c2 * _pts[i+3] + c3 * t1y + c4 * t2y;

            //store points in array
            res.push(x);
            res.push(y);
        }
    }
    return res;
}

function drawLines(ctx, pts) {
    ctx.moveTo(pts[0], pts[1]);
    // -
    for(let i=2; i<pts.length-1; i+=2){
        ctx.lineTo(pts[i], pts[i+1]);
    }
}

// - Save file
function drawFrame( frequencyArray, callBack ) {
	ctx.save();
	ctx.clearRect(0,0, opts.width, opts.height);

	let gradient = null;
	if( !bgImg ){
		// - Background gradient
		gradient = ctx.createRadialGradient(opts.width*0.5, opts.height*0.5, opts.width*0.1, opts.width*0.5, opts.height*0.5, opts.width );
		// Add three color stops
		gradient.addColorStop(0, '#3F3E43');
		gradient.addColorStop(1, '#000000');

		// Set the fill style and draw a rectangle
		ctx.fillStyle = gradient;
		ctx.fillRect(0,0, opts.width, opts.height);
	}

	// - Blur (En back no va)
	// - ctx.filter = `blur(${opts.blur}px)`;

	
	// - Waves
	waves.forEach( (wave, i) =>{
		ctx.globalCompositeOperation = opts.waves.blendMode;
		// -
		l = opts.waves.points * 2;
		percentFrequency = ( frequencyArray.length / 2 / (wave.points.length * waves.length) );
		
		// -
		let j 			= 0,
			maxY 		= 0,
			absolutePos = 0,
			velPos 		= 0,
			vel 		= 0;
			// -
		for( j=0; j<l; j+=2){
			// - x
			wave.points[j] = j*incrPoint*0.5;
			// - 
			absolutePos = (i*l)+j;

			// - Ajustamos con una sinusoidal para que los graves no destaquen tanto y se parezca mas al diseño
			shapeIncr = 1 - (Math.abs(Math.cos(j/(l*opts.waves.sinusoidal*2))) * opts.waves.sinusoidalPercent);

			// - 
			/*
			wave.proxyPoints[j+1] = opts.height - (Math.random() * opts.height); 
			/*/
			wave.proxyPoints[j+1] = opts.height - (((frequencyArray[Math.floor(percentFrequency*absolutePos)]) * opts.height * (opts.waves.multiplier/opts.waves.layers))*shapeIncr*(1-(absolutePos*opts.waves.incr*-1))) * opts.volume;
			//*/
			// - if(j==0) console.log( wave.proxyPoints[j+1], wave.points[j+1]);
			vel = wave.proxyPoints[j+1] < wave.points[j+1] ? opts.velUp : opts.velDown;
			wave.points[j+1] += (wave.proxyPoints[j+1] - wave.points[j+1]) * vel; // - y
			// -
			// maxY = Math.max( maxY, wave.points[j+1]) || 0;
		}

		// - Draw
		gradient = ctx.createLinearGradient(0, 0, 0, opts.height );
		// Add three color stops
		gradient.addColorStop(0, opts.waves.colorRGBA );
		gradient.addColorStop(opts.waves.gradient, opts.waves.colorRGBA );
		gradient.addColorStop(opts.waves.gradient2, opts.waves.colorRGBA2 );
		gradient.addColorStop(1, opts.waves.colorRGBA2 );
		ctx.fillStyle = gradient;
		ctx.fill();		
		

		// - Stroke
		ctx.strokeStyle = opts.waves.lineColorRGBA;
		ctx.lineWidth = opts.waves.lineWidth;
		ctx.stroke();

		// - Drop Shadow
		ctx.shadowOffsetY = 0;
		ctx.shadowOffsetY = opts.shadow.distance * -1;
		ctx.shadowColor = opts.shadow.colorRGBA;
		ctx.shadowBlur = opts.shadow.blur;

		// -
		drawCurvePath(ctx, wave.points, opts.waves.tension, true, 8 );

		// -
		ctx.globalCompositeOperation = 'source-over';
		
	});
	
	// -
	ctx.restore();
	// -
	// - Convert to PNG
	const filename = `${tmpFolder}/frame-${addZeros(frame, 3)}.png`;
	// -
	const buf = canvas.toBuffer('image/png');
	// Create the file
	const file = Buffer.from( buf, 'base64');
	fs.writeFile(filename, file, function(err) {
		if(err) return console.error(err);// - If an error occurred, show it and return
		// - console.log(`The PNG ${indexFrame} file was created.`);
	});
	// -
	frame++;
	indexFrame += step;
	// -
	setTimeout(callBack, opts.delay );
}



// -  Get audio
function decodeSoundFile(soundfile){
	dataIni = new Date();
	console.log("Decoding audio file ", soundfile, "...");
	// - 
	fs.readFile(soundfile, function(err, buf) {
		if (err){
			console.log(err);
			throw err;
		}
		// -
		audioCtx.decodeAudioData(buf, function(audioBuffer) {
			// - console.log( audioBuffer.duration );
			pcmdata = audioBuffer.getChannelData(0);
			drawWaveform( audioBuffer.sampleRate );

		}, function(err) { throw err })
	})
}

// - 
let indexFrame = 0,
	step = 1,
	pcmdata = null,
	outputNameAuto = 'waveform';
// - 
function drawWaveform( _samplerate){
	samplerate = _samplerate;
	step = Math.floor( samplerate / opts.fps );
	indexFrame = opts.iniTime * samplerate;
	// -
	dataIniDecode = new Date();
	// - 
	console.log( samplerate, indexFrame, opts.endTime*samplerate, pcmdata.length, step );
	// -
	//loop through song in time with sample rate
	drawNextFrame();
}
function drawNextFrame(){
	// - console.log( indexFrame );
	if (indexFrame >= Math.min(pcmdata.length, opts.endTime*samplerate) ) {
		// - Convert video
		convertVideo();
		return;
	}else{
		drawFrame( pcmdata.slice(indexFrame, indexFrame+step), drawNextFrame );
	}
	
}
// - 

const audioFile = `${__dirname}/audios/${args[0] || 'test.mp3'}`;
decodeSoundFile(audioFile);
// - 

function convertVideo(){
	console.log("Convert video...");
	dataIniConvertVideo = new Date();
	// -
	const re = /^(.+)\/(.+)[\.]{1}(.+)$/gi;
	const inputFileName = re.exec( audioFile )[2].replace(' - ', '_').replace(' ', '_').toLowerCase();
	// - 
	outputNameAuto = `${inputFileName}_${opts.oWidth}x${opts.oHeight}(${opts.width}x${opts.height})_${opts.fps}fps_${opts.iniTime}-${opts.endTime}_(${frame}f)_${opts.waves.layers}_b${opts.blur}_p${preset}`;
	// - console.log( outputNameAuto );
	outputFile = `${videosFolder}/${outputNameAuto}.mp4`;



	
	// -
	const finalH = 896;
	// - 
	const complexFilters = [];
	complexFilters.push({
		filter: 'scale',
		options: { 
			w: opts.oWidth, 
			h: finalH
		},
		inputs: '1:v',
		outputs: '[bg]'
	});
	// -
	complexFilters.push({
		filter: 'scale',
		options: { 
			w: opts.oWidth, 
			h: opts.oHeight
		},
		inputs: '2:v',
		outputs: '[frames]'
	});
	// -
	if(opts.blur > 0 ){
		complexFilters.push({
			filter: 'boxblur',
			options: opts.blur,
			inputs: 'frames',
			outputs: '[frames]'
		});
	}
	// -
	complexFilters.push({
		filter: 'overlay',
		options: { 
			x: 0,
			y: finalH-opts.oHeight
		},
		inputs: ['bg', 'frames'],
		outputs: '[outv]'
	});
	
	// -
	ffmpeg()
	.input(			audioFile )
	.seekInput(		opts.iniTime )
	.input(			bgImg ? `${imgFolder}/${bgImg}` : null )
	.loop(			1 )
	.input(			`${tmpFolder}/frame-%03d.png` )
	.inputFPS(		opts.fps )
	.complexFilter(	complexFilters )
	.output(		outputFile )
	.duration(		opts.endTime-opts.iniTime )
	.videoBitrate(	opts.bitrate )
	.outputFPS(		opts.fps )
	.outputOptions([
		'-map 0:a:0',
		'-map [outv]',
		'-shortest'])
	.outputOptions( '-metadata', 	'title="Título del podcast"' )
    .outputOptions( '-c:a',			'aac' )
	.outputOptions(	'-b:a',			'320k' )
    .outputOptions( '-f',			'mp4' )
	.outputOptions( '-coder',		'1' )
	.outputOptions( '-pix_fmt',		'yuv420p' )
	.outputOptions( '-level',		'3.0' )
	.outputOptions( '-profile:v',	'high' )
    .outputOptions( '-movflags',	'+faststart' )
	.outputOptions( '-loglevel',	'error' )
	.videoCodec(	'libx264')
	.on('progress', onProgress )
	.on('end', 		onConverted )
	.on('error', 	onError )
	.run();
}
// -
function onConverted(){
	// - Borramos carpeta tmp
	if (fs.existsSync(tmpFolder) && opts.removeTmpFolder ) fs.rmdirSync(tmpFolder, { recursive: true });
	// -
	dataEnd = new Date();
	// - 
	console.log(`Video creado con éxito 👏👏👏`);

	// - Input size
	const statsFile = fs.statSync( audioFile )
	const fileSizeInBytes = statsFile.size;

	// - Output size
	const statsFileOutput = fs.statSync( outputFile )
	const fileSizeInBytesOutput = statsFileOutput.size;
	
	// - Log
	const tableLog = {
		/*samplerate: 		samplerate,*/
		width: 				opts.oWidth,
		height: 			opts.oHeight,
		scale: 				opts.scale,

		fps: 				opts.fps,
		/*duration:			dateFormated(time2obj((opts.endTime-opts.iniTime)*1000)),*/
		layers: 			opts.waves.layers,
		blur: 				opts.blur,
		preset: 			preset,
		
		time2DecodeFile: 	dateFormated(time2obj(timeDifference( dataIniDecode.getTime(), dataIni.getTime()) )),
		time2DrawPNGs: 		dateFormated(time2obj(timeDifference( dataIniConvertVideo.getTime(), dataIniDecode.getTime()) )),
		totalFrames: 		frame,
		totalTime: 			dateFormated(time2obj(timeDifference( dataEnd.getTime(), dataIni.getTime()) )),
		
		outputFile: 		`${outputNameAuto}.mp4`,
		outputSize: 		`${(fileSizeInBytesOutput / (1024*1024)).toFixed(3)} Mb`
	};
	console.table( tableLog );
	console.log( Object.values(tableLog).toString() );
}

// -
function onProgress(progress){
	console.log( JSON.stringify(progress) );
}
function onError(e){
	console.log(e);
}


// - Functions
function addZeros( n, length ){
    //- Length: Número de xifres que volem que tingui el nou número
    const str = (n > 0 ? n : -n) + "";
    let zeros = "";
    for (let i = length - str.length; i > 0; i--){
        zeros += "0";
    }
    zeros += str;
    return n >= 0 ? zeros : "-" + zeros;
}

// - 
function time2obj( _time ){
    const o = {};
    if( _time < 0 ){
        o.negative = true;
       _time *= -1;
    }
    // -
    o.seconds = Math.floor((_time)/1000),
    o.minutes = Math.floor(o.seconds/60),
    o.hours   = Math.floor(o.minutes/60),
    o.days    = Math.floor(o.hours/24)
    //-
    o.hours   = o.hours-(o.days*24);
    o.minutes = o.minutes-(o.days*24*60)-(o.hours*60);
    o.seconds = o.seconds-(o.days*24*60*60)-(o.hours*60*60)-(o.minutes*60);
    //-
    return o;
}
// - 
function timeDifference( _date1, _date2 ){
    return Number(_date1) - Number(_date2);
}
// - 
function dateFormated( o ){
	let str = '';
	// -
	if( o.hours > 0 ) str += `${addZeros(o.hours, 2)}:`;
	str += `${addZeros(o.minutes, 2)}:`;
	str += `${addZeros(o.seconds, 2)}`;
	// -
	return str;
}