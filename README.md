# Crear mp4 del waveform en Node.js

```bash
node create.js 'test.wav' 13.0 22.3 25 600 320
```
---

### Arguments:
| 				| 0  			| 1 		        | 2			        | 3  			| 4  				| 5  				| 6  				| 7  				| 8  				| 9  				| 10			|
| --------------| ------------- | ----------------- | ----------------- | ------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ----------------- | ------------- |
| `propiedad`	| Input file	| Incio (seconds)	| Final (seconds) 	| FPS			| Ancho	(px)		| Alto (px)			| Capas  			| Escala  			| Blur  			| Preset  			| Background 	|
| `default`		| 'test.mp3'	| 0.0 		        | 10.0 		        | 25			| 256				| 128				| 7  				| 1.0  				| 0.0  				| 3  				| ''			|
| `tipo`		| String 		| Number 			| Number 			| Number		| Number 			| Number 			| Number  			| Number  			| Number  			| Number  			| String 		|
---

### Input file
Nombre del archivo de audio. Debe estar dentro de la carpeta *audios*.

### Input background file (opcional)
Nombre de la imagen de fondo. Debe estar dentro de la carpeta *img*.


---
Los archivos PNG se iran creando en la carpeta **tmp** (si no existe se creará). Al finalizar se creará el video (MP4) en la carpeta **videos**, y la carpeta **tmp** se eliminará automáticamente.

---


# Crear en serie todos los videos *(60, 48, 30, 25, 20, 15, 10) fps*
### 600 x 320 px (1 minuto)
```bash
npm run createAll_60_600
```
### 600 x 320 px (2 minutos)
```bash
npm run createAll_120_600
```
### 512 x 128 px (1 minuto)
```bash
npm run createAll_60_512
```
### 512 x 128 px (2 minutos)
```bash
npm run createAll_120_512
```
### 256 x 96 px (1 minuto)
```bash
npm run createAll_60_256
```
### 256 x 96 px (2 minutos)
```bash
npm run createAll_120_256
```
---

# Crear en serie todos los videos de escalas de 0.1 a 1.0
### 512 x 128 px (1 minuto / 3 capas / preset waveform 3)
```bash
npm run createAll_scales_3
```

# Crear en serie todos los videos de escalas de 0.1 a 1.0
### 512 x 128 px (1 minuto / 3 capas / preset waveform 4)
```bash
npm run createAll_scales_4
```

---
# Crear en serie todos los videos con desenfoques de 0.0 a 5.0
### 512 x 128 px (1 minuto / 3 capas / escala 0.4)
```bash
npm run createAll_blurs
```
---