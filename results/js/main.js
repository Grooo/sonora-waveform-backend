function addGroupTable( tables, args ){
	const sufix = args.sufix || '';
	const id = `gt-${args.w}x${args.h}${sufix}`;
	const groupDom = d3.select(".tables").append("div").classed('table-group', true).attr('id', id);
	const titleDom = groupDom.append("div").classed('title', true).append("h2").text( `${args.w} x ${args.h}` );
	const subTitleDom = titleDom.append("span").text( `(${args.w*args.h} px)` );
	// - 
	tables.forEach((obj)=>{
		addCSVTable( `t-${args.w}x${args.h}-${obj.minutes*60}${sufix}`, `results_${args.w}x${args.h}-${obj.minutes*60}${sufix}.csv`, `0${obj.minutes}:00`, groupDom );
	});
};

function addCSVTable(id, file, title, el ){
	const tableContainer = el.append("div").classed('table-container', true).attr('id', id);
	const tableHeader = tableContainer.append("header");
	const titleDom = tableHeader.append("h3").html( `${title}` );
	const subTitleDom = tableHeader.append("a").attr('href', `data/${file}`).attr('target', `_blank`).text( `csv` );
	// - 
	const table = tableContainer.append("table"),
		thead = table.append("thead"),
		tbody = table.append("tbody")
	// - 
	let outputFileCol = -1,
		totalTimeCol = -1;
	// - 
	d3.csv(`data/${file}`, function (data, i) {
		// - console.log(data, i);
		let columns = [];
		if (i === 0) {
			columns = Object.keys(data);
			// - 
			thead.append("tr")
				.selectAll("th")
				.data(columns)
				.enter()
				.append("th")
					.text((column, j) => {
						if( column === 'outputFile' ) outputFileCol = j;
						if( column === 'totalTime' ) totalTimeCol = j;
						return column;
					});
		}
		// -
		columns = Object.values(data);
		// - 
		tbody.append("tr")
			.selectAll("td")
			.data(columns)
			.enter()
			.append("td")
				.html((column, j) => {
					if( j === totalTimeCol ){
						return `<span class="big">${column}</span>`;
					}else if( j === outputFileCol ){
						return `<a href="${path}videos/${column}" target="_blank">video</a>`;
					}
					return column;
				});
		
	});
}